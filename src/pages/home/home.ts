import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

export interface Shizzle {
  title: string;
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  shizzleCollectionRef: AngularFirestoreCollection<Shizzle>;
  shizzle$: Observable<Shizzle[]>;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public actionSheetCtrl: ActionSheetController, public afs: AngularFirestore) {
    this.shizzleCollectionRef = this.afs.collection<Shizzle>('shizzles');
    this.shizzle$ = this.shizzleCollectionRef.snapshotChanges().map(actions => {
      return actions.map(action => {
        const data = action.payload.doc.data() as Shizzle;
        const id = action.payload.doc.id;
        return { id, ...data };
      });
    });
  }

    createShizzle(shizzle: Shizzle) {
      let prompt = this.alertCtrl.create({
      title: 'Shizzle Name',
      message: "Enter a name for this new shizzle you're so keen on adding",
      inputs: [
        {
          name: 'title',
          placeholder: 'Title'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            this.shizzleCollectionRef.add({ title: data.title });
          }
        }
      ]
    });
    prompt.present();
  }    

  updateShizzle(shizzle: Shizzle) {
    let prompt = this.alertCtrl.create({
      title: 'Shizzle Name',
      message: "Update the name for this shizzle",
      inputs: [
        {
          name: 'title',
          placeholder: 'Title',
          value: shizzle.title
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
    this.shizzleCollectionRef.doc(shizzle.id).update({ title: data.title });
          }
        }
      ]
    });
    prompt.present();
  }

  deleteShizzle(shizzle: Shizzle){
    this.shizzleCollectionRef.doc(shizzle.id).delete();
  }

  goToOtherPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.navCtrl.push(ContactPage);
  }

  showOptions(shizzle: Shizzle) {
    console.log(shizzle);
    console.log(this.shizzleCollectionRef.doc(shizzle.id));

    let actionSheet = this.actionSheetCtrl.create({
      title: 'What do you want to do?',
      buttons: [
        {
          text: 'Delete Shizzle',
          role: 'destructive',
          handler: () => {
            this.deleteShizzle(shizzle);
          }
        },{
          text: 'Update Shizzle',
          handler: () => {
            this.updateShizzle(shizzle);
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
